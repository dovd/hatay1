<!doctype html>
<html lang="vi">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="{{asset('frontend/img/favicon.ico')}}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Shopeelike | Trang chủ</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="{{asset('frontend/css/bootstrap.css')}}" rel="stylesheet" />
        <link href="{{asset('frontend/css/landing-page.css')}}" rel="stylesheet"/>

        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="{{asset('frontend/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '248934005588352');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=248934005588352&ev=PageView&noscript=1"
        /></noscript>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127115330-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-127115330-1');
        </script>
    </head>
    <body class="landing-page landing-page2">
        <nav class="navbar navbar-transparent navbar-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                    </button>
                    <a href="#">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="{{asset('frontend/img/new_logo.png')}}" alt="Creative Tim Logo">
                            </div>
                            <div class="brand">
                                ShopeeLike Team
                            </div>
                        </div>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="example" >
                    <ul class="nav navbar-nav navbar-right">
                    	<li>
                            <a href="#tinhnang">
                             Tính năng
                            </a>
                        </li>
                        <li>
                            <a href="#giathue">
                             Giá thuê
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/shopeeshare/" target="_blank">
                            <i class="fa fa-facebook-square"></i>
                             Fanpage
                            </a>
                        </li>
                        <li>
                        	<a href="mailto:shopeelike.contact@gmail.com"><i class="fa fa-envelope-o"> </i> Hỗ trợ</a>
                        </li>
                        <li>
                        	<a href="tel:0973861122"><i class="fa fa-phone"> </i> Hotline: 0973861122</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="wrapper">
            <div class="parallax filter-gradient blue" data-color="blue">
                <div class= "container">
                    <div class="row">
                        <div class="col-md-7  hidden-xs">
                            <div class="parallax-image">
                                <img src="{{asset('frontend/img/showcases/showcase-2/mac1.png')}}"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="description text-center">
                                <h2>ShopeeLike app</h2>
                                <br>
                                <h5>Treo máy, Trao đổi theo dõi và yêu thích trên SHOPEE.
                                 <br />
                                 Quản lý nhiều shop chỉ trên 1 màn hình ứng dụng.
                                 <br />
                                 Đẩy sản phẩm 1 cách tự động 
                                 <br />
                                 Clone tài khoản 1 cách nhanh nhất 
                                </h5>
                                <div class="buttons">
                                    <a href="{{config('shopeelike.downloads.macos')}}" class="btn btn-fill btn-neutral" target="_blank">
                                    	<i class="fa fa-apple"></i> Tải về macos
                                    </a>
                                      <a href="{{config('shopeelike.downloads.windowns')}}" class="btn btn-fill btn-neutral" target="_blank">
                                    	<i class="fa fa-windows"></i> Tải về windows
                                    </a>
                                </div>
                                <small>* Đăng ký trực tiếp trên app</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-gray section-clients">
                <div class="container text-center">
                    <h4 class="header-text"></h4>
                    <p>
                    </p>
                </div>
            </div>
            <div class="section section-features" id="tinhnang">
                <div class="container">
                    <h4 class="header-text text-center">Tính năng</h4>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card card-blue">
                                <div class="icon">
                                    <i class="pe-7s-share"></i>
                                </div>
                                <div class="text">
                                    <h4>Trao đổi</h4>
                                    <p>
                                    	Sau mỗi "6 phút" bạn sẽ được trao đổi 1 lượt theo dõi và yêu thích tất cả sản phẩm với shop khác.
                                    	<br /> 
                                    	Bạn sẽ được treo tối đa 2 tài khoản cùng một thời điểm. 
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-blue">
                                <div class="icon">
                                    <i class="pe-7s-signal"></i>
                                </div>
                                <h4>Quản lý nhiều tài khoản</h4>
                                <p>
                                	Thao tác trên nhiều tài khỏa shopee với 1 màn hình ứng dụng duy nhất.
                                	<br />
                                	Thông báo khi có order từ tài khoản shopee về ứng dụng.
                                	<br />
                                	Chat trực tiếp với khách trên shopee bằng ứng dụng.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-blue">
                                <div class="icon">
                                    <i class="pe-7s-server"></i>
                                </div>
                                <h4>Clone shopee</h4>
                                <p>Bạn có thể nhanh chóng xuất toàn bộ sản phẩm của shop chỉ định ra excel. 
                                <br />Công việc tiếp theo bạn chỉ việc sử dụng chức năng nhập sản phẩm của shopee.vn</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-blue">
                                <div class="icon">
                                    <i class="pe-7s-stopwatch"></i>
                                </div>
                                <h4>Đẩy sản phẩm</h4>
                                <p>Hẹn giờ để thực hiện chức năng đẩy sản phẩm của shopee.vn</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-gray section-clients">
                <div class="container text-center">
                    <h4 class="header-text"></h4>
                    <p>
                    </p>
                </div>
            </div>
            <div class="section section-features" id="giathue">
                <div class="container">
                    <h4 class="header-text text-center">Giá thuê</h4>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="card card-blue">
                                <div class="icon">
                                    <i class="pe-7s-cash"></i>
                                </div>
                                <div class="text">
                                    <h4><b>149k</b> /1 Tháng</h4>
                                    <p class="text-left">
                                    	- Vui lòng chuyển tiền vào <b>Vietcombank</b>
                                    	
                                    </p>
                                     <p class="text-left" style="margin-top: 10px">
                                     - Số tài khoản: <b>0691000308944</b>
                                     </p>
                                      <p class="text-left" style="margin-top: 10px">
                                     - Chủ tài khoản: <b>Nguyễn Văn Hà</b>
                                     </p>
                                      <p class="text-left" style="margin-top: 10px">
                                     - Nội dung: <b>"tài khoản-số tháng" (ví dụ: xxx@gmail.com-6)</b>
                                     </p>
                                      <p class="text-left" style="margin-top: 10px">
                                     - Sau 15' tài khoản của bạn sẽ được gia hạn
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-no-padding">
                <div class="parallax filter-gradient blue" data-color="blue">
                    <div class="parallax-background">
                        <img class ="parallax-background-image" src="{{asset('frontend/img/showcases/showcase-2/bg2.jpg')}}">
                    </div>
                    <div class="info">
                        <h1>Tải Shopeelike app</h1>
                        <p>Treo máy, Trao đổi theo dõi và yêu thích trên SHOPEE.
                                 <br />
                                 Quản lý nhiều shop chỉ trên 1 màn hình ứng dụng.
                                 <br />
                                 Đẩy sản phẩm 1 cách tự động 
                                 <br />
                                 Clone tài khoản 1 cách nhanh nhất 
                                </p>
                        <div class="buttons">
                                    <a href="{{config('shopeelike.downloads.macos')}}" class="btn btn-fill btn-neutral" target="_blank">
                                    	<i class="fa fa-apple"></i> Tải về macos
                                    </a>
                                      <a href="{{config('shopeelike.downloads.windowns')}}" class="btn btn-fill btn-neutral" target="_blank">
                                    	<i class="fa fa-windows"></i> Tải về windows
                                    </a>
                                </div>
                                <small>* Đăng ký trực tiếp trên app</small>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                Trang chủ
                                </a>
                            </li>
                            <li>
                                <a href="#tinhnang">
                                Tính năng
                                </a>
                            </li>
                            <li>
                                <a href="#giathue">
                                Giá thuê
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="social-area pull-right">
                        <a class="btn btn-social btn-facebook btn-simple" target="_blank" href="https://www.facebook.com/shopeeshare/">
                        	<i class="fa fa-facebook-square"></i>
                        </a>
                        <a class="btn btn-social btn-email btn-simple" href="mailto:shopeelike.contact@gmail.com"><i class="fa fa-envelope-o"> </i></a>
                        
                    </div>
                    <div class="copyright">
                        &copy; 2018 <a href="https://www.facebook.com/shopeeshare/">Shopeelike Team</a>, đồng hành là cách để thành công
                    </div>
                </div>
            </footer>
        </div>
    </body>
    <script src="{{asset('frontend/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/js/jquery-ui-1.10.4.custom.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/js/bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('frontend/js/awesome-landing-page.js')}}" type="text/javascript"></script>
</html>
