import request from '../utils/request'

export function fetchList(query) {
  return request({
    url: '/user/list',
    method: 'get',
    params: query
  })
}

export function active(id) {
  return request({
    url: '/user/'+id+'/active',
    method: 'post'
  })
}
export function unactive(id) {
  return request({
    url: '/user/'+id+'/unactive',
    method: 'post'
  })
}

export function expire(id , params) {
  return request({
    url: '/user/'+id+'/expire',
    method: 'post',
    params
  })
}

export function summary() {
  return request({
    url: '/user/summary',
    method: 'get'
  })
}
