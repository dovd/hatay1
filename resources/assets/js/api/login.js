import request from '../utils/request'

export function loginByEmail(email, password) {
  const data = {
    email,
    password
  }
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function loginByFacebook(token) {
	  const data = {
		token
	  }
	  return request({
	    url: '/login/facebook',
	    method: 'post',
	    data
	  })
}

export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: '/auth/user',
    method: 'get',
    params: { token }
  })
}

