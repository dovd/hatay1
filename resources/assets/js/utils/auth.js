import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
	var now = new Date();
	now.setTime(now.getTime() + 1 * 3600 * 1000);
	return Cookies.set(TokenKey, token,{ expires: now })
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
