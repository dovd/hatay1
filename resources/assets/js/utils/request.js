import axios from 'axios'
import { Message } from 'element-ui'
import store from '../store'
import { getToken } from './auth'
import Cookies from 'js-cookie'

// create an axios instance
const service = axios.create({
  baseURL: process.env.MIX_BASE_API, // api base_url
  timeout: 15000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // Do something before request is sent
    if (store.getters.token) {
    	config.headers['Authorization'] = 'Bearer ' + getToken();
    }
    //location
    config.headers['X-localization'] = Cookies.get('language') || 'en';
    return config
  },
  error => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
  }
)

// respone interceptor
service.interceptors.response.use(
  response => response,
  error => {
    if(error.response.status !== 422){
    	Message({
	      message: error.message,
	      type: 'error',
	      duration: 5 * 1000
	    })
	    console.log(error)
    }
    return Promise.reject(error)
  }
)

export default service
