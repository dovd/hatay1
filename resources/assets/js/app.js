import Vue from 'vue'
import router from './router'
import i18n from './lang'
import ElementUI from 'element-ui'
import App from './App.vue' 
import store from './store'

import '@/styles/index.scss'
import 'normalize.css/normalize.css' 
import 'element-ui/lib/theme-chalk/index.css';
import './icons'
import './errorLog' 
import './vue-permission'
import * as filters from './filters' 
//import './assets/fb_sdk.js'
 
  
Vue.use(ElementUI, {
	  size: 'medium', // set element-ui default size
	  i18n: (key, value) => i18n.t(key, value)
}) 

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})


new Vue({
  el: '#app',
  router,
  i18n,
  store,
  render: h => h(App)
})