import router from './router'
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getToken } from './utils/auth'
import { removeToken } from './utils/auth'

NProgress.configure({ showSpinner: false })

function hasPermission(roles, permissionRoles) {
  if (roles.indexOf('admin') >= 0) return true // admin permission passed directly
  if (!permissionRoles) return true
  return roles.some(role => permissionRoles.indexOf(role) >= 0)
}
const whiteList = ['/login','/']
router.beforeEach((to, from, next) => {
	NProgress.start();
	if (getToken()) { 
		 if (to.path === '/login') {
	      next({ path: '/dashboard' })
	      NProgress.done() 
	    }else{
	    	if (store.getters.roles.length === 0) { 
	            store.dispatch('GetUserInfo').then(res => { 
	              const roles = res.data.data.roles // note: roles must be a array! such as: ['editor','develop']
	              store.dispatch('GenerateRoutes', { roles }).then(() => { 
	                router.addRoutes(store.getters.addRouters) 
	                next({ ...to, replace: true }) // set the replace: true so the navigation will not leave a history record
	              })
	            }).catch((err) => {
	              store.dispatch('FedLogOut').then(() => {
	                Message.error(err || 'Verification failed, please login again')
	                next({ path: '/login' })
	              })
	            })
	          } else {
	            if (hasPermission(store.getters.roles, to.meta.roles)) {
	              next()
	            } else {
	              next({ path: '/401', replace: true, query: { noGoBack: true }})
	            }
	          }
	    }
	}else{
		if (whiteList.indexOf(to.path) !== -1) { 
	      next()
	    } else {
	      next('/login')
	      NProgress.done()
	    }
	}
})


router.afterEach(() => {
  NProgress.done();
})