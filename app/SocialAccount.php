<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    protected $fillable = ['user_id', 'provider_user_id', 'provider','access_token' , 'access_token_expires_at'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
