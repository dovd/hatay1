<?php

namespace App\Http\Controllers;


use App\ShopeePage;
use App\Shops;
use App\Shop;
use App\Http\Requests\ShopFormRequest;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Follow;
use App\UserBalance;
use App\Transaction;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\DefaultShopFormRequest;

class ShopController extends Controller
{
    public function createOrUpdate(ShopFormRequest $r)
    {
        $shop = Shop::whereId($r -> id) -> first();
        if(!isset($shop)){
            $shop = new Shop();
        }
        $shop -> id = $r -> id;
        $shop -> name = $r -> name;
        $shop -> save();
        return response([
            'status' => 'success',
            'data' => $shop
        ], 200);
    }
    
    
    public function point($shop_from , $shop_to){
        DB::beginTransaction();
            $POINT_SUB = 1;
            $user = auth() -> user();
            $shopTo = Shop::whereId($shop_to) -> first();
            $userBalance = UserBalance::whereShopId($shop_from) ->first();
            $userShopToBalance = UserBalance::whereShopId($shop_to) ->first();
            
            $now = new Carbon();
            $isCheckDate = false;
            if($userBalance -> updated_at == null){
                $isCheckDate == true;
            }else{
                $update =  new Carbon($userBalance -> updated_at);
                $fdate = $update -> addMinute('5' , $userBalance -> updated_at);
                if($now > $fdate){
                    $isCheckDate == true;
                }
            }
            if($userShopToBalance -> balance_point < $POINT_SUB && !$isCheckDate){
                return response([
                    'status' => 'success',
                    'point' => 0
                ], 200);
            }
            $userBalance -> balance_point += $POINT_SUB;
            $userShopToBalance -> balance_point -=$POINT_SUB;
            
            $userBalance -> save();
            $userShopToBalance -> save();
          
            $tranCode = Uuid::uuid1()->toString();
            Transaction::create([
                'user_id' => $user -> id,
                'transaction_code' => $tranCode,
                'credit_point' => $POINT_SUB
            ]);
            Transaction::create([
                'user_id' => $shopTo -> user_id,
                'transaction_code' => $tranCode,
                'debit_point' => $POINT_SUB
            ]);
        DB::commit();
        return response([
            'status' => 'success',
            'data' =>  array(['point' => $POINT_SUB])
        ], 200);
    }
    
    public function follow($shop_from , $shop_to ,Request $request){
        DB::beginTransaction();
        if($request -> is_point){
            $POINT_SUB = 1;
            $user = auth() -> user();
            
            $userBalance = UserBalance::whereShopId($user -> default_shop_id) ->first();
            $userBalance -> balance_point += $POINT_SUB;
            $userBalance -> save();
            
            $shopTo = Shop::whereId($shop_to) -> first();
            $useTo = User::whereId($shopTo -> user_id) -> first();
            $userShopToBalance = UserBalance::whereShopId($useTo -> default_shop_id) ->first();
            $userShopToBalance -> balance_point -=$POINT_SUB;
            $userShopToBalance -> save();
//             $now = new Carbon();
//             $isCheckDate = false;
//             if($userBalance -> updated_at == null){
//                 $isCheckDate == true;
//             }else{
//                 $update =  new Carbon($userBalance -> updated_at);
//                 $fdate = $update -> addMinute('5' , $userBalance -> updated_at);
//                 if($now > $fdate){
//                     $isCheckDate == true;
//                 }
//             }
//             if($userShopToBalance -> balance_point < $POINT_SUB && !$isCheckDate){
//                 return response([
//                     'status' => 'success',
//                     'point' => 0
//                 ], 200);
//             }
            $tranCode = Uuid::uuid1()->toString();
            Transaction::create([
                'user_id' => $user -> id,
                'transaction_code' => $tranCode,
                'credit_point' => $POINT_SUB
            ]);
            Transaction::create([
                'user_id' => $shopTo -> user_id,
                'transaction_code' => $tranCode,
                'debit_point' => $POINT_SUB
            ]);
        }
        $follow = DB::table('follows') 
        -> where('shop_id_from' , $shop_from)
        -> where('shop_id_to' , $shop_to)
        -> first();
        if(!isset($follow)){
            $follow = new Follow();
            $follow -> shop_id_from = $shop_from;
            $follow -> shop_id_to = $shop_to;
            $follow -> save();
        }
        DB::commit();
        return response([
            'status' => 'success',
            'data' => $follow
        ], 200);
    }
    
    public function setDefaultShop(DefaultShopFormRequest $request){
        DB::beginTransaction();
        $user = auth() -> user();
        $shop = new Shop();
        $shop -> id = $request -> shopid;
        $shop -> name =  $request -> shopname;
        $shop -> user_id = $user -> id;
        $shop -> save();
        $balances = UserBalance::where('user_id' , $user -> id) -> get();
        $balance = UserBalance::where('user_id' , $user -> id) -> where('shop_id' , $shop -> id) -> first();
        if(!isset($balance)){
            $balance = new UserBalance();
            $balance -> user_id = $user -> id;
            $balance -> shop_id =  $request -> shopid;
            if($request -> is_default){
                $balance -> balance_point = 1;
            }else{
                $balance -> balance_point = 0;
            }
            $balance -> save();
            $balances[] = $balance;
        }
        if($request -> is_default){
          $user -> default_shop_id = $request -> shopid;
          $user -> save();
        }
        $user -> balances = $balances;
        $user -> point = 0;
        for ($i = 0; $i < count($balances); $i++) {
            if($balances[$i] -> shop_id == $user -> default_shop_id){
                $user -> point = $balances[$i] -> balance_point;
                break;
            }
        }
        DB::commit();
        return response([
            'status' => 'success',
            'data' =>  ['shop' => $shop , 'user' => $user]
        ], 200);
    }
    
    
    public function getSubShop($shop_running){
        $shop = new Shop();
        $shop -> id  = -1;
        $shop -> name  = '';
        $shop_ = DB::table('users')
        -> join('shops as s', 's.id', '=', 'users.default_shop_id')
        -> leftJoin('user_balances as b', 's.id', '=', 'b.shop_id')
        -> leftJoin('follows as f', 's.id', '=', 'f.shop_id_from')
        -> select('s.id' , 's.name')
        -> where('s.id' ,'!=', $shop_running)
        -> where('b.balance_point' ,'>', 0)
        -> orderBy('s.updated_at')
        -> whereNotIn('s.id' ,function ($query) use ($shop_running) {
             $query -> select('f.shop_id_to')
             -> from('follows as f')
             -> where('f.shop_id_from' ,'=', $shop_running);
        })
        -> first();
        if(isset($shop_)){
            $shop = $shop_;
        }
        if( $shop -> id  != -1){
            DB::table('shops')
            -> where('id' ,'=', $shop -> id)
            -> update(['updated_at' => new \DateTime()]);
        }
        return response([
            'status' => 'success',
            'data' => $shop
        ], 200);
    }
}
