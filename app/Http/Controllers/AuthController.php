<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\RegisterFormRequest;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Facebook\Facebook;
use App\SocialAccount;
use App\Http\Requests\LoginFormRequest;
use Carbon\Carbon;
use App\UserBalance;
use Illuminate\Support\Facades\DB;
use App\ExpireHistories;
use App\Http\Requests\ExpireFormRequest;

class AuthController extends Controller
{
    
    public function register(RegisterFormRequest $request)
    {
        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }
    
    public function registerClient(RegisterFormRequest $request)
    {
        DB::beginTransaction();
        $expiry_date = new \DateTime('now +30 day');
        $user = new User();
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->expiry_date = $expiry_date;
        $user->status = 1;
        $user->status_name = 'ACTIVE';
        $user->save();
        DB::commit();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }
    
    public function login(LoginFormRequest $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            $credentials['is_admin'] = '1';
            if (!$token = auth() -> attempt($credentials)) {
                return response([
                    'email' => ['Email hoặc password không hợp lệ. Vui lòng kiểm tra lại'],
                ], 422);
            }
            return response([
                'status' => 'success',
                'token' => $token,
                'data' => auth()->user()
            ]);
        } catch (\Exception $e) {
            return response([
                'status' => 'error',
                'msg' => $e ->getMessage()
            ]);
        }
    }
    
    
    public function loginClient(LoginFormRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $credentials['status'] = '1';
        if (!$token = auth() -> attempt($credentials)) {
            return response([
                'email' => ['Email hoặc password không hợp lệ. Vui lòng kiểm tra lại'],
            ], 422);
        }
        $today = date("Y-m-d H:i:s");
        if (new \DateTime() > new \DateTime(auth()->user()->expiry_date) ) {
            return response([
                'date' => ['Hết hạn sử dụng. Vui lòng truy cập "http://shopeelike.com/#giathue/" để gia hạn tài khoản'],
            ], 422);
        }
        $user = auth()->user();
        $balance = UserBalance::where('user_id' , $user->id) -> get();
        $user -> balances = $balance;
        $user -> point = 0;
        for ($i = 0; $i < count($balance); $i++) {
            if($balance[$i] -> shop_id == $user -> default_shop_id){
                $user -> point = $balance[$i] -> balance_point;
                break;
            }
        }
        return response([
            'status' => 'success',
            'token' => $token,
            'data' => $user
        ]);
    }
    
    public function listUsers(Request $request){
        $query = User::select('*');
        //::where('id','!=',auth()->user() -> id) 
        //::get();
        $list = $query -> forPage($request -> page,$request -> limit) -> get();
        $total = $query -> count();
        return response([
            'items' => $list,
            'total' => $total
        ]);
    }
    
    public function active($id){
        $user = User::whereId($id) -> first();
        if(!isset($user)){
            return response([
                'status' => 'error',
                'message' => 'Not found.'
            ]);
        }
        if($user -> status == 0){
            $user -> status = 1;
            $user -> status_name = 'ACTIVE';
        }
        $user -> save();
        return response([
            'status' => 'success',
            'message' => 'Active is success.'
        ]);
    }
    
    public function summary(){
        $userQuery = User::where('is_admin' , '!=' , '1');
        $total = $userQuery -> count();
        $active = $userQuery -> where('status' , '1') -> count();
        $unactive = $total - $active;
        $expired = $userQuery
        -> select(DB::raw('count(distinct users.id) as total_expire'))
        -> whereNotNull('e.user_id')
        -> groupBy('users.id')
        -> leftJoin('expire_histories as e', 'e.user_id', '=', 'users.id')
        -> first();
        return response([
            'status' => 'success',
            'user' => [
                'total' => $total,
                'active' => $active,
                'unactive' => $unactive,
                'expired' => isset($expired) ? $expired -> total_expire : 0,
            ]
        ]);
    }
    
    public function unactive($id){
        $user = User::whereId($id) -> first();
        if(!isset($user)){
            return response([
                'status' => 'error',
                'message' => 'Not found.'
            ]);
        }
        if($user -> status == 1){
            $user -> status = 0;
            $user -> status_name = 'UNACTIVE';
        }
        $user -> save();
        return response([
            'status' => 'success',
            'message' => 'UnActive is success.'
        ]);
    }
    
    public function expiryDate($id , ExpireFormRequest $request){
        DB::beginTransaction();
        $user = User::whereId($id) -> first();
        if(!isset($user)){
            return response([
                'status' => 'error',
                'message' => 'Not found.'
            ]);
        }
        $expiry_date = $expiry_date_from = new \DateTime('now');
        $current_expire = (new \DateTime($user -> expiry_date));
        if($expiry_date < $current_expire){
            $expiry_date = $current_expire;
            $expiry_date_from = (new \DateTime($user -> expiry_date));
        }
        $expiry_date -> add(new \DateInterval('P'.($request -> days + (30*$request -> months)).'D'));
        $user -> expiry_date = $expiry_date;
        $user -> save();
        ExpireHistories::create([
            'user_id' => $id,
            'expire_date_from' => $expiry_date_from,
            'expire_date_to' => $expiry_date,
            'days' => $request -> days,
            'months' => $request -> months,
        ]);
        DB::commit();
        return response([
            'status' => 'success',
            'message' => 'Expire date is success.'
        ]);
    }
    
    
    public function loginWithSocial(Request $request , $social , Facebook $fb)
    {
        $uid = null;
        $email = null;
        $name = null;
        $user = null;
        $account = null;
        $longLivedAccessToken = null;
        if($social == 'facebook'){
            try {
                $fb->setDefaultAccessToken($request -> token);
                $response = $fb->get('/me?fields=email,name');
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                return response([
                    'common' => ['Graph returned an error: ' . $e->getMessage()],
                ], 422);
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                return response([
                    'common' => ['Facebook SDK returned an error: ' . $e->getMessage()],
                ], 422);
            }
            $me = $response->getGraphUser();
            $uid = $me->getId();
            $email = $me->getEmail();
            $name =  $me->getName();
            $oAuth2Client = $fb->getOAuth2Client();
            $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($request -> token);
            $fb->setDefaultAccessToken($longLivedAccessToken);
        }
       
        $account = SocialAccount::whereProvider($social)
        ->whereProviderUserId($uid)
        ->first();
        if(!isset($account)){
            $account = new SocialAccount([
                'provider_user_id' => $uid,
                'provider' => $social,
                'access_token' => $longLivedAccessToken ->getValue(),
                'access_token_expires_at' => $longLivedAccessToken ->getExpiresAt(),
            ]);
            $user = new User([
                'email' => $email,
                'name' => $name,
                'is_social_account' => '1' 
            ]);
            $user -> save();
            $account->user()->associate($user);
            $account->save();
        }else{
            SocialAccount::whereProvider($social)
            ->whereProviderUserId($uid)
            ->update([ 
                'access_token' => $longLivedAccessToken ->getValue(),
                'access_token_expires_at' => $longLivedAccessToken ->getExpiresAt()
            ]);
        }
        $token = JWTAuth::fromUser($account -> user);
        return response([
            'status' => 'success',
            'token' => $token
        ]);
    }
    
    public function user(Request $request)
    {
        $user = auth()->user();
        //balance = UserBalance::whereUserId($user -> id) ->first();
        //$user -> point = $balance -> balance_point;
        if($user -> is_admin == '1'){
            $user -> roles =  [ "admin" ];
        }
        return response([
            'status' => 'success',
            'data' => $user
        ]);
    }
    
    public function logout()
    {
        auth()->logout(true);
        return response([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }
    
    public function refresh()
    {
        $token = JWTAuth::getToken();
        $newToken = JWTAuth::refresh($token);
        return response([
            'status' => 'success',
            'token' => $newToken
        ]);
    }
}
