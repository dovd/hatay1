<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Facades\JWTAuth;
use Closure;
use Exception;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Illuminate\Support\Facades\Auth;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['error' => 'token_invalid', 'message' => 'Token missing or badly formatted'] , 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                $token = JWTAuth::parseToken() ->refresh();
                $user = JWTAuth::setToken($token)->toUser();
                Auth::login($user , false);
            }else{
                return response()->json(['error' => 'token_not_provided', 'massage' => 'Token expired', 401]);
            }
        }
        $response = $next($request);
        if(isset($token)){
            return $this->setAuthenticationHeader($response, $token);
        }else{
            return $response;
        }
        
    }
}
