<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Hash;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->hasHeader('X-emai') && $request->hasHeader('X-password')){
            $user = User::whereEmail($request->header('X-emai'));
            if (Hash::check($request->header('X-password'), $user -> password)){
                $request ->query('user' , $user);
            }else{
                exit;
            }
        }else{
            exit;
        }
        return $next($request);
    }
}
