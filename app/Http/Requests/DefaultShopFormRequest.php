<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DefaultShopFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shopid' => 'required|unique:shops,id',
            'shopname' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'shopid.unique' => 'Tài khoản shopee đã được 1 tài khoản khác trên hệ thống sử dụng.',
        ];
    }
}
