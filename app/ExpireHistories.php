<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpireHistories extends Model
{
    protected $fillable = [
        'user_id', 'expire_date_from', 'expire_date_to', 'days', 'months'
    ];
}
