<?php

return [
    'downloads' => [
        'windowns' => env('SHOPEELIKE_DOWNLOADS_WINDOWNS' , '#'),
        'macos' => env('SHOPEELIKE_DOWNLOADS_MACOS', '#'),
    ],
];
