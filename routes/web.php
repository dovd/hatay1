<?php

use Facebook\Facebook;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    




Route::get('/adm/{any}', function () {
    return view('app');
}) ->where('any', '.*') ->name('home'); 

Route::get('/{any}', function () {
    return view('home');
}) ->where('any', '.*') ->name('fontend'); 

