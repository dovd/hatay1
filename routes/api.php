<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'localization'], function(){
    //Route::post('signup', 'AuthController@register');
    Route::post('registerClient', 'AuthController@registerClient');
    Route::post('login', 'AuthController@login');
    Route::post('loginClient', 'AuthController@loginClient');
    Route::post('login/{social}', 'AuthController@loginWithSocial');
    Route::group(['middleware' => ['jwt.verify']], function(){
        Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');
        Route::get('shop/{shop_running}/sub/' , 'ShopController@getSubShop');
        Route::post('shop' , 'ShopController@createOrUpdate');
        Route::post('shop/{shop_from}/point/{shop_to}' , 'ShopController@point');
        Route::post('shop/{shop_from}/follow/{shop_to}' , 'ShopController@follow');
        Route::post('shop/default' , 'ShopController@setDefaultShop');
        Route::group(['middleware' => ['admin']], function(){
            Route::get('user/list', 'AuthController@listUsers');
            Route::post('user/{id}/active', 'AuthController@active');
            Route::post('user/{id}/unactive', 'AuthController@unactive');
            Route::post('user/{id}/expire', 'AuthController@expiryDate');
            Route::get('user/summary', 'AuthController@summary');
        });
    });
    Route::get('auth/refresh', 'AuthController@refresh');
});

